# -*- coding: utf-8 -*-
from collections import namedtuple

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Accomm, Attendee, Food


SUBJECT = "DebConf20 Moving online: existing registrations cancelled"

TEMPLATE = """\
Dear {{ name }},

The DebConf team has had to take the hard decision that DebConf 20
cannot happen in-person, in Haifa, in August, as originally planned.
This decision is based on the status of the venue in Haifa, the local
team's view of the local health situation, the existing travel
restrictions and the [results of the survey we ran][0].

[0]: https://salsa.debian.org/debconf-team/public/data/dc20/-/tree/master/survey

What are we doing instead?

We're holding DebConf 20 online!

We can still get together to share our ideas, discuss plans in Birds of
a Feather sessions, and eat cheese, from the safety of our desks at home.

What this means for you in practice:

Since there are no travel, accommodation, or bursaries for the online DebConf,
we've decided not to hold on to people's original registrations for DebConf20.
Once we re-open registrations for the online version, you'll need to
re-register.  We apologise for any inconvenience this may cause you, and thank
you for your understanding!

So, please [submit your talk, sprint, and BoF proposals][1] for DebConf
20 Online.

[1]: https://debconf20.debconf.org/cfp/

It will be held within the same dates, as before, 23-29 August.  We expect the
event to be significantly shorter than a usual DebCamp + DebConf, but that will
depend on the volume of proposals we receive.

Hopefully in 2021 we can once again hold conferences in person, with
DebConf 21 taking place in Haifa.  In that case, all of the planned DebConfs
would be held a year later than originally scheduled: 2022 in Kosovo and 2023
in Kochi, India.

See you online in August!

The DebConf Team
"""

class Command(BaseCommand):
    help = 'Send moving online emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        ctx = {
            'name': name,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])

        for attendee in queryset:
            self.badger(attendee, dry_run)
