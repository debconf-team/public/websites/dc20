---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>


| **FEBRUARY**          |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 26 (Wednesday)        | Opening of the [Call For Proposals](/cfp/)                           |


| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 5 (Sunday)            | Last day for submitting a talk that will be considered for the official schedule |
| 26 (Sunday)           | Last day to register for guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |


| **AUGUST**            |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 16 (Sunday)           | Last date for presenters to submit pre-recorded talks to the video team. |
| 23 (Sunday)           | DebConf day 1                                                        |
| 24 (Monday)           | DebConf day 2                                                        |
| 25 (Tuesday)          | DebConf day 3                                                        |
| 26 (Wednesday)        | DebConf day 4                                                        |
| 27 (Thursday)         | DebConf day 5                                                        |
| 28 (Friday)           | DebConf day 6                                                        |
| 29 (Saturday)         | DebConf day 7                                                        |
