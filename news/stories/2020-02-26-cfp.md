---
title: Call for Proposals
---

The Call for Proposals for DebConf20 has been [published][].

[published]: /cfp/

You can now sign in to this website and submit your talk.
The first batch of accepted proposals will be published in April.

We look forward to seeing you in Haifa!
