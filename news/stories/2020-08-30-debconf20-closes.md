---
title: DebConf20 online closes
---

On Saturday 29 August 2020, the annual Debian Developers
and Contributors Conference came to a close.

DebConf20 has been held online for the first time, due to the coronavirus 
(COVID-19) disease pandemic. 

All of the sessions have been streamed, with a variety of ways of participating:
via IRC messaging, online collaborative text documents,
and video conferencing meeting rooms.

With more than 850 attendees from 80 different countries and a
total of over 100 event talks, discussion sessions,
Birds of a Feather (BoF) gatherings and other activities,
[DebConf20](https://debconf20.debconf.org) was a large success.

When it became clear that DebConf20 was going to be an online-only
event, the DebConf video team spent much time over the next months to
adapt, improve, and in some cases write from scratch, technology that
would be required to make an online DebConf possible. After lessons
learned from the MiniDebConfOnline in late May, some adjustments were
made, and then eventually we came up with a setup involving Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad, and a newly written web-based
frontend for voctomix as the various elements of the stack.

All components of the video infrastructure are free software, and the
whole setup is configured through their public
[ansible](https://salsa.debian.org/debconf-video-team/ansible) repository.

The DebConf20 [schedule](https://debconf20.debconf.org/schedule/) included
two tracks in other languages than English: the Spanish language MiniConf,
with eight talks in two days,
and the Malayalam language MiniConf, with nine talks in three days.
Ad-hoc activities, introduced by attendees over the course of the entire conference,
have been possible too, streamed and recorded. There have also been several
team gatherings to [sprint](https://wiki.debian.org/Sprints/) on certain Debian development areas.

Between talks, the video stream has been showing the usual sponsors on the loop, but also
some additional clips including photos from previous DebConfs, fun facts about Debian
and short shout-out videos sent by attendees to communicate with their Debian friends. 

For those who were not able to participate, most of the talks and sessions are already
available through the
[Debian meetings archive website](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/),
and the remaining ones will appear in the following days.


The [DebConf20](https://debconf20.debconf.org/) website
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.


Next year, [DebConf21](https://wiki.debian.org/DebConf/21) is planned to be held
in Haifa, Israel, in August or September.


DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Community team)
have been available to help so participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the [web page about the Code of Conduct in DebConf20 website](https://debconf20.debconf.org/about/coc/)
for more details on this.


Debian thanks the commitment of numerous [sponsors](https://debconf20.debconf.org/sponsors/)
to support DebConf20, particularly our Platinum Sponsors:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com/)
and 
[**Amazon Web Services (AWS)**](https://aws.amazon.com/).


### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
_universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).


### About Lenovo

As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, [**Lenovo**](https://www.lenovo.com)
understands how critical open systems and platforms are to a connected world.

### About Infomaniak

[**Infomaniak**](https://www.infomaniak.com) is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 


### About Google

[**Google**](https://google.com/) is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts 
of [Salsa](https://salsa.debian.org)'s continuous integration infrastructure
within Google Cloud Platform.


### About Amazon Web Services (AWS)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) is one of the world's
most comprehensive and broadly adopted cloud platforms,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.
